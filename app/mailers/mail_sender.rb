class MailSender < ActionMailer::Base
  default from: "no-reply@cafedasmatas.com.br"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mail_sender.talk_to_us.subject
  #
  def talk_to_us(name, email, message)
    @name = name
    @email = email
    @message = message

    mail to: 'contato@cafedasmatas.com.br', reply_to: @email
  end
end
