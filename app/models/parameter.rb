class Parameter < ActiveRecord::Base
  attr_accessible :contact_to, :pagseguro_email, :pagseguro_token
  validates_presence_of :contact_to, :pagseguro_email, :pagseguro_token
end
