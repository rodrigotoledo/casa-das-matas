class TalkToUs
  include ActiveAttr::Model
  include ActiveAttr::TypecastedAttributes
  
  attribute :name
  attribute :email
  attribute :message
  validates_presence_of :name, :email, :message
  
  attr_accessible :name, :email, :message

  def send_mail
    return false unless self.valid?

    MailSender.talk_to_us(self.name,self.email,self.message).deliver
  end
end