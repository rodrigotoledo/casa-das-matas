class OrderProduct < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  attr_accessible :price, :quantity, :product_id
  validates_presence_of :product_id, :price, :quantity
  validates_numericality_of :quantity, greater_than: 0
  validates_numericality_of :price, greater_than: 0

  def name
    "#{self.product.name rescue '-'} | Pedido: #{self.order.name rescue '-'}"
  end
end
