class Page < ActiveRecord::Base
  attr_accessible :content, :title, :url
  validates :title, presence: true, uniqueness: true
  validates :url, presence: true, uniqueness: true
  validates :content, presence: true
end
