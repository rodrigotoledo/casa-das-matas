module PostsHelper
  def month_name(month)
    I18n.t('date.abbr_month_names')[month]
  end
end
