class PagesController < ApplicationController
  def show
    @page = Page.where(url: params[:id]).last
    raise unless @page
  rescue
    redirect_to root_path
  end
end
