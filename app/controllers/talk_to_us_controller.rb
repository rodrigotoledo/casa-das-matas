class TalkToUsController < ApplicationController
  def index
    @talk_to_us = TalkToUs.new
  end

  def send_mail
    @talk_to_us = TalkToUs.new(params[:talk_to_us])
    @talk_to_us.send_mail
    render nothing: true
  end
end
