class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.has_attached_file :image
      t.string :title
      t.text :description
      t.string :url
      t.integer :position
      t.string :localization

      t.timestamps
    end
  end
end
