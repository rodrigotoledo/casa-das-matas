class CreateParameters < ActiveRecord::Migration
  def change
    create_table :parameters do |t|
      t.string :contact_to
      t.string :pagseguro_token
      t.string :pagseguro_email

      t.timestamps
    end
  end
end
