class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.float :total
      t.string :client_email
      t.string :client_name
      t.boolean :finished

      t.timestamps
    end
  end
end
